angular.module('app')

.config(function($stateProvider){
	$stateProvider

	.state("root",{
		url: '/', // what the url should look like
		component: 'root'
	});

})

.component('root',{
	template: '<div>' + '<ab-navbar></ab-navbar>' + 
				'<ab-main></ab-main>' +
				'<ab-footer></ab-footer>' + '</div>'
});