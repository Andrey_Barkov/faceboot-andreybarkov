

angular.module('app')

.component('abPosts',{
	template: `
	<div ng-repeat="post in Posts">
	<!-- USER POST-->
        <div class="post" >
            <!--USER POST HEADER-->
            <div class="post__header flex-between margin-16">
                <a class="profile profile--lg profile--hover-ul" href="#">
                    <img src="./assets/stock_profile.jpg">
                    <span>{{post.username}}</span>
                </a>
                <span class="timestamp">{{post.published_at | date}}</span>
            </div>
            <!--USER POST CONTENT-->
            <div class="text-md margin-16">
                {{post.content}}
            </div>
            <!--USER POST FOOTER-->
            <div class="flex-between margin-16">
                <div class="flex">
                    <a class="btn-icon btn--hover-red" id="like" ng-click="toggle = !toggle; like(post, toggle)" ng-class="{'red' : toggle}"><i class="fa fa-heart fa-lg"></i></a>
                    <span>{{post.likes}}</span>
                </div>
                <div class="flex">
                    <a class="btn-icon" href="#"><i class="fa fa-comments-o fa-lg"></i></a>
                    <span>{{post.replies.length}}</span>
                </div>
            </div>
	            <ul class="margin-16 top-divider">
					<div class="comments">
                    <div ng-repeat="comment in post.replies">
                		<!--USER COMMENT-->
                		<li class="flex-col">
                        <!--USER COMMENT HEADER-->
                        <div class="flex-between">
                            <a class="profile profile--hover-ul" href="#">
                                <img src="./assets/stock_profile.jpg">
                                <span>{{comment.username}}</span>
                            </a>
                            <span class="timestamp text-sm">{{comment.published_at | date }}</span>
                        </div>
                        <!--USER COMMENT CONTENT-->
                        <p class="text-md margin-8-top">
                            {{comment.content}}
                        </p>
                    </li>
                    </ul>
                    <div class="p-input">
                    <form ng-submit="update(post)"> 
                    <input type="text" class="p-input__ta" placeholder="Add a comment..." ng-model="value" id="post">
                    <input class="btn btn-primary f-right margin-8-v" type="submit" id="submit" ng-click="update(value, post); $scope.value='null';" value="POST"/>
                    
                    </form>
	            </div>`,

	  
	controller: function($scope, $rootScope, $http, UserService) {
		$http.get("http://private-36ae7-bootcamp3.apiary-mock.com/questions").then(function (response) {
        $scope.Posts = response.data;
        console.log($scope.Posts);
        $scope.like=function(post, flag){

        	if(flag){
        		post.likes=post.likes+1;
        	}else{
        		post.likes=post.likes-1;
        	}
        }
         $scope.update = function(value, post){
         	var newpost = {username:"Jenkins9000", content:value, published_at:new Date()};
        	post.replies.push(newpost);
        	document.getElementById("post").value = "";


    };
		})}


});