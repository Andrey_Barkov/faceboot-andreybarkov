angular.module('app')

.component('abUser',{
	template:'<div>' +
			'<div class="profile profile--lg profile--hover-ul" ng-click="myAlert()">' +
                    '<img src="./assets/stock_profile.jpg">' +
                    '<span>{{username}}</span>' +
               ' </div>' +
               '<span class="timestamp text-sm">May 30 at 9:59pm</span>'+
               '</div>',

               controller: function($scope){
               		$scope.username = "Username";
               		$scope.myAlert = function(){
               			window.alert($scope.username);
               		};

               }
});