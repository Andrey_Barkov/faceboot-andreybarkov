angular.module('app')

.component('abMain',{
	template: '<audio controls autoplay>'+
			  	'<source src="./assets/RussianEmpireAnthem.mp3" type="audio/mpeg">'+
			  '</audio>'+
			  '<div class="container">' + 
    			'<div class="content">'+
    				'<ab-posts></ab-posts>'+
    				
    			'</div> '+ 
    			'<div class=aside>' +
					'<ab-aside></ab-aside>' +
				'</div>'+ 
    	    '</div>'
});