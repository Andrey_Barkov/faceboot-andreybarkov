require('./assets/styles/index.css');
require('angular');
require('angular-ui-router');
require("json-loader!./users.json");

/**
 * Insert your code here !
 */
angular.module('app',['ui.router'])

.config(function($stateProvider,$locationProvider,$urlRouterProvider){
	$locationProvider.html5Mode(true); // getting rid of # in URL (!#)
	$urlRouterProvider.otherwise('/'); // if unknown -> goto "/"
	$stateProvider

})
.service('UserService', function () {
  this.Users = null;
})
	.controller(function($scope, $http, UserService, $rootScope) {
    $http.get("users.json").then(function (response) {
        $scope.Users = response.data;
        UserService.Users = Users;
        $rootScope.$broadcast('postsFetched');
    });
})


const context = require.context('.',true,/\.component\.js$/);
context.keys().forEach(context); 
